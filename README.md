# M183-Alex-Schmid

- [Keylogger](#01-keylogger)
- [Clickjacking](#02-clickjacking)
- [Two Factor Authentication](#03-twofactor)
- [Password Validator](#06-password-validator)
- [HTTP Digest Authentication](#07-http-digest-authentication)


# 01 Keylogger
Jede Taste die gedrückt wird, wird vom Programm erfasst.<br/>
Nach Leerschlägen wird ein Wort gebildet und nach Punkten und nachdem Enter gedrückt wurde wird ein Satz gebildet.<br/>
Dieser Satz wird an https://m183.gibz-informatik.ch/api/xssKeylogs mit dem POST Formular geschickt.

# 02 Clickjacking
Beim Clickjacking handelt es sich um eine Angriffsmethode, bei der der User auf eine falsche Webseite geleitet wird, die aber dank einem eingebundenen iFrame genau wie die vermeintliche Webseite aussehen soll.<br/>
Der User denkt dann, dass er seine Credentials in die vermeintliche Webseite eintippt, in Wirklichkeit gibt er sie aber auf unserer Webseite ein und wir schicken sie direkt nach https://m183.gibz-informatik.ch/clickjacked-credentials.<br/>
Der User wird danach noch auf eine Error-Page weitergeleitet, damit er nicht merkt, dass er gerade gehackt wurde.<br/>
*Leider nicht Responsive gestaltet, bei 1546px Breite, stimmt das Form mit dem unterliegenden Überein.*

# 03 TwoFactor
Bei der 2-Faktor Authentifikation muss sich der Benutzer zuerst mit korrekten Credentials (username = "Admin", password = "password") einloggen, bevor er auf eine weitere Seite weitergeleitet wird.<br />
Dort muss er mit einer App (z.B. Google Authenticator) den erscheinenden QR-Code einscannen oder man kann den generiretten Key auch manuel in der Authenifizierugnsapp eingeben.<br />
In der App wird dann anhand des QR-Codes oder des Keys ein Code generiert, welche alle 30 Sekunden erneuert wird. Diesen Code muss zur Authentifikation eingetippt werden.
Ist das Token korrekt, hat sich der Benutzer korrekt eingeloggt und wird auf eine kleine Bestätigungsseite weitergeleitet.<br />
Somit ist der 1. Faktor die Credentials und der 2. Faktor der QR-Code. 

# 06 Password Validator
Beim Password validator wird ein Passwort überprüft und muss einige Anforderungen erfüllen, damit es nicht zu einfach ist. Nach der Überprüfung muss das Passwort verschlüsselt werden, damit eine sichere Speicherung des Passwortes gewährleistet ist.<br/>
Das Passwort kann nur abgesendet werden, nach erfüllen aller Punkte, alle Punkte sind ersichtlich. Nach dem Absenden, wird die verschlüsselte Version auf der Seite angezeigt.<br/>
Wir haben das Ganze mit npm packages und WebPack gemacht, damit es einen sauberen Aufbau hat und realitätsgetreu ist.<br/>
Bei WebPack wird der Inhalt generiert aus dem Index.html und aus dem Index.js, der generierte Inhalt wird im Main.js gespeichert.<br/>
Nach jeder änderung muss das File gespeichert werden und im root-Verzeichnis  ```npx webpack``` in der Konsole ausgeführt werden, dieser Befehl bildet das gesamte Projekt neu und kann über das Index.html geöffnet werden.

<b>Welche Anforderungen an das Passwort bestehen? Abwägung zwischen Sicherheit und Nutzungsfreundlichkeit!</b><br/>
Das Passwort muss natürlich eine gewisse Länge besitzen, je mehr desto besser. Wir haben uns für eine Mindestlänge von 8 Zeichen entschieden.<br/>
Es sollte ebenfalls nicht nur aus Kleinbuchstaben bestehen sondern sollte Klein- und Grossbuchstaben beinhalten. <br/>
Um zusätzliche Komplexität hinzuzufügen, muss es Zahlen und Sonderzeichen beinhalten (Folgende Sonderzeichen können verwendet werden: !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/? )<br/>
Wir finden, dass wir mit dieser Kombination aus den verschiedenen Anforderungen ein sicheres Passwort gewährleisten können, bei dem allerdings die Nutzungsfreundlichkiet nicht zu stark sinkt.<br/>

<b>Wie wird das Passwort in der Datenbank gespeichert?</b><br/>
Das Passwort sollte nie als Plain Text in der Datenbank gespeichert werden. Wir hashen das Passwort bevor es in die Datenbank kommen würde. Der Hash wird mit der Technick SHA256 gemacht. Es ist mit Abstand der weit verbreiteste Hash und somit mit den meisten Systemen kompatibel.<br/><br/>
<b>Dieses Projekt wurde mit Timo Gloor gemacht</b><br/>

# 07 HTTP Digest Authentication
Es soll auf einen Server zugegriffen werden können, um sich anzumelden. Beim Versuch der Anmeldung wird als allererstes eine HTTP-Request ausgeführt, die mit dem Fehler 401(Unauthorized) zurückkommen soll, da noch keine Credentials mitgeschickt worden sind.<br>
Mit der Antwort vom Server werden sogenannte WWW-Authenticate Headers zurückgeschickt, mit denen man sich dann anschliessend authentifizieren kann.<br>
In diesem Header ist ein Nonce(<b>N</b>umber only used <b>once</b>), der aus dem aktuellen TimeStamp und einem Hash besteht, enthalten, ein Server realm der normalerweise den Namen des Hosts beinhaltet, der Hash algorithmus(meist MD5) und die QOP(meist auth)<br>
Danach werden 3 Hashes generiert mit MD5. Der erste Hash (HASH1) sieht so aus: "Benutzername:ResponseRealm:Passwort".<br> 
Der zweite Hash (HASH2) sieht so aus: "HTTP-Request-Methode:LokalerPfad".<br>
Die Response, welche danach wieder zurück an den Server geschickt wird sieht so aus: HASH1:ResponseNonce:NonceCount:ClientNonce:ResponseQOP:HASH2.<br>
Anmeldedaten, welche vorgegeben waren:<br>
britney_s : hitMeBaby<br>
bon_jovi : badMedicine<br>
shakira : hipsDontLie<br>
tom_j : sexbomb<br>

# 09 CSRF Attacken
Bei diesen Aufgaben ging es darum, verschiedene CSRF Attacken durchzuführen. Durch das Aufrufen von einer Seite eines Drittanbieters, kann ein Angreifer Daten ändern oder abgreifen, wenn der Benutzer eingeloggt ist und die Seite nicht dagegen geschützt ist.<br/> 
Bei der Aufgabe 1 war der Angriff sehr einfach, da keinerlei Schutz verfügbar war. Es musste nur zuerste der Benutzername mit einer Request geholt werden.<br />
Danach konnte man ganz einfach mit einer Request changePassword, das Passwort ändern.<br />
Die nächsten beiden Aufgaben, waren vom Exploit her gleich, sie unterschieden sich lediglich im Body und dem URL der Request.<br/>
Bei Aufgabe 2, wurde im Body die neue email mitgeschickt und bei Aufgabe 3 die Nachricht.<br />
<b>Bei meiner Umsetzung funktioniert das Programm lediglich, wenn es auf einem Webserver läuft.</b>

# 10 Informationsquellen
Es wurden recherchen zu Verschiedenen Themen und Quellen durchgeführt.<br/>
Genaueres dazu im Verzeichnis 10_Informationsquellen.<br/>