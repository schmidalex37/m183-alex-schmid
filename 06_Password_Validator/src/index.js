import * as SHA256 from 'crypto-js/sha256';

// String wird gehasht
function encrypt(hashableString) {
	return SHA256(hashableString);
}
// input Feld für das Passwort
var hashPassword = document.getElementById('input_password');
// Button zum Registrieren
var submitBtn = document.querySelector('#register_btn');

// Erkennt wenn der Registrier-Button gedrückt wird und das Passwort wird verschlüsselt und ausgegeben
submitBtn.addEventListener('click', () => document.querySelector('#hash_container').innerHTML = encrypt(hashPassword.value));

// Registrier-Button soll erst verfügbar sein, sobald alle Anforderungen erfüllt wurden
document.getElementById("register_btn").disabled = true;

// Anforderungsliste
var pw_requirements_list = ["8 Zeichen", "Zahlen", "Kleinbuchstaben", "Grossbuchstaben", "Sonderzeichen"];

// Alle Anforderungen im HTML anzeigen
for (var i = 0; i < pw_requirements_list.length; i++) {
	document.getElementById("pw_requirements").innerHTML += ('<li id="' + pw_requirements_list[i] + '">' + pw_requirements_list[i] + '</li>');
}
				
var pw = document.getElementById("input_password");

// Event, welches nach jedem Input in der Passwort-Box ausgeführt wird
pw.addEventListener("input", () => {
								
	// Passwort muss länger als 8 Zeichen sein
	UpdateElements(0, ((pw.value.length > 8) ? "none" : "list-item"));

	// Passwort muss Zahlen enthalten
	UpdateElements(1, (/\d/.test(pw.value) ? "none" : "list-item"));
		
	// Passwort muss Kleinbuchstaben enthalten
	UpdateElements(2, (/[a-z]/.test(pw.value) ? "none" : "list-item"));
		
	// Passwort muss Grossbuchstaben enthalten
	UpdateElements(3, (/[A-Z]/.test(pw.value) ? "none" : "list-item"));
		
	// Passwort muss Sonderzeichen enthalten            
	UpdateElements(4, (/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(pw.value) ? "none" : "list-item"));

	// Alle Anforderungen erfüllt?
	document.getElementById("register_btn").disabled = !CheckValidation();
});


function UpdateElements(childNr, value)
{
	// Update display value of the elements
	document.getElementById("pw_requirements").children[childNr].style.display = value;
}


function CheckValidation()
{
	var count = 0;
	for(var i = 0; i < pw_requirements_list.length; i++)
	{
		// Zählen der Elemente, welche bereits erfüllt wurden => display == "none"
		if(document.getElementById(pw_requirements_list[i]).style.display == "none") { count++; } 
	}
	return (count == i);
}        
