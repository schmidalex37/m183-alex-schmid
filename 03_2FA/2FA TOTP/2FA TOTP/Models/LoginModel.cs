﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2FA_TOTP.Models
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        
        public LoginModel(string username, string password)
        {
            Username = username;
            Password = password;
        }
        public LoginModel() { }
    }
}