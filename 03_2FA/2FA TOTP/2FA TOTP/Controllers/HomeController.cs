﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;
using _2FA_TOTP.Models;
using Google.Authenticator;
using System.Timers;

namespace _2FA_TOTP.Controllers
{
    public class HomeController : Controller
    {
        private const string Key = "aaaabbbbccccdddd";
        private string action = "Login";
        
        public int Interval { get; set; }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            ViewBag.Status = false;
            if (login.Username == "Admin" && login.Password == "password")
            {
                Session["Username"] = login.Username; //Set session with username
                ViewBag.Status = true;
                action = "Token"; // Redirect to Token
            }
            else
            {
                action = "Login"; //Wrong Credentials
            }
            return RedirectToAction(action, "Home");
        }
        public ActionResult Token()

        {
            ViewBag.Message = "Use Google Authenticator App to Scan the QR Code:";
            var authenticator = new TwoFactorAuthenticator();
            var result = authenticator.GenerateSetupCode("M183 2FA", "2FA", Key, 300, 300);
            ViewBag.BarcodeImageUrl = result.QrCodeSetupImageUrl; // QR Code
            ViewBag.SetupCode = result.ManualEntryKey;

            return View();
        }
        public ActionResult UserProfile() // Confirmation Site
        {
            if (Session["Username"] == null || Session["IsValid2FA"] == null || !(bool)Session["IsValid2FA"])
            {
                return RedirectToAction("Login");
            }
            ViewBag.Message = $"Welcome {Session["Username"]}. You have logged in successfully!";
            return View();
        }
        public ActionResult Verify2Fa() // Verification of TwoFactor Code
        {
            var token = Request["passcode"];
            var authenticator = new TwoFactorAuthenticator();
            var isValid = authenticator.ValidateTwoFactorPIN(Key, token);
            if (isValid)
            {
                Session["IsValid2FA"] = true;
                return RedirectToAction("UserProfile", "Home");
            }
            return RedirectToAction("Login", "Home");
        }
    }
}