- [Computerworld](#computerworld)
- [Heise](#heise-security)
- [Spiegel](#spiegel)



# Computerworld

### Quelle
Die erste Quelle, die ich ausgewählt habe, ist von Computerworld.<br /> 
Die Quelle ist aus meiner Sicht gut für IT-Sachen, da sich die gesamte Quelle auf diese Sachen bezieht und zusätzlich sieht man genau wann es geschrieben worden ist und wer der Autor des Artikels ist.<br />
Alle Informationen die für die Zusammenfassung weiter unten verwendet wurden, sind von dem folgenden Link: <br />
[https://www.computerworld.ch/security/phishing/hacker-nehmen-office-365-admins-visier-2369152.html](https://www.computerworld.ch/security/phishing/hacker-nehmen-office-365-admins-visier-2369152.html)

### Kurzzusammenfassung
Über eine breit angelegte Phishing-Attacke wollen Hacker an die Zugangsdaten der Admins gelangen, besonders Firmenkunden sollen davon betroffen sein.<br />
Für die Angriffe werden E-Mail adressen von seriösen Unternehmen verwendet, so entgehen sie dem Spam-Filter. <br /> 
In diesen Phishing-Mails, werden die Admins dazu aufgefordert, ihre Zahlungsinformationen zu aktualisieren. Der im Mail enthaltene Link, führen zu einer gefälschten Office 365 Login Seite.<br />
Diese Konten sind für Angreifer sehr wertvoll, da vielfach in diesen Dokumenten kritische Firmendaten gespeichert sind.<br /> 
Indikatoren für einen solchen Angriff sind komische Absender und nicht typische URL's.<br />

### Reflexion
Phishing-Attacken sind sehr verbreitet und eine einfache Art und Weise einen Angriff zu versuchen. Das Prinzip eines solchen Angriffes beruht darauf, dass eine Person leichtfertig ist und Links in Mails anklickt ohne richtig zu schauen.<br />
Normal sollte dies nicht passieren, denn solche Links sollten bei jeder Person Alarmglocken auslösen, ist aber leider nicht immer so.<br />

### Was ist neu ? Was wusste ich schon ?
Hier ist mir nichts neu, denn dieses Beispiel einer Phishing-Attacke auf Office 365 Konten ist ein "normaler" Phishing-Versuch, wie er im Buch beschrieben steht.<br />
Da ich mich mit Phishing-Angriffen auskenne, ist mir hier nichts neu.<br />

### Was hat sie erstaunt ?
Mich erstaunt jedes mal aufs neue, wie man so leichtfertig sein kann und diesen komischen Links folgen kann, obwohl man genau weiss, dass man es nicht machen sollte.<br />
Schon alleine beim anschauen der Absender Mail-Adresse sollte man realisiseren, dass es sich hier nicht um eine Mail von Microsoft handelt, wenn es Zahlen darin hat und eine komische Domain.<br />

### Wie schätze ich die Bedeutung dieser Information für mich persönlich, für meinen Beruf, für meinen Betrieb, für die Gesellschaft, ... ein?
Da wir in unserem Betrieb solche Schulungen haben und einen guten Spam-Filter besitzen, kommt selten eine Phishing-Mail durch.<br />
Ich finde es wichtig, dass die Leute solche Schulungen absolvieren, denn Phishing-Attacken können verherend sein und eigentlich sind sie so einfach zu vermeiden.<br />
Die Bedeutung dieser Information ist, aus meiner Sicht, für alle Berufe relevant, da alle davon betroffen sein könnten.<br />

### Inwiefern werden Sie Ihr Verhalten/Handeln aufgrund dieser Information ändern bzw. anpassen?
Mein Handel ändert sich nicht, da ich bereits jetzt grundsätzlich nicht auf Links in Mails klicke und schon gar nicht wenn der Absender eine Reihe von Zahlen und Buchstaben beihnaltet, was meist bei solchen Attacken der Fall ist.<br />

# Heise Security

### Quelle
Die erste Quelle, die ich ausgewählt habe, ist von Heise.<br /> 
Die Quelle wirkt auf mich seriös, da sie ihre Quellen angibt und es ebenfalls ein Magazin ist.<br />
Alle Informationen die für die Zusammenfassung weiter unten verwendet wurden, sind von dem folgenden Link: <br />
[https://www.heise.de/newsticker/meldung/Streamingdienst-Tausende-Disney-Konten-werden-in-Untergrundforen-gehandelt-4587863.html](https://www.heise.de/newsticker/meldung/Streamingdienst-Tausende-Disney-Konten-werden-in-Untergrundforen-gehandelt-4587863.html)

### Kurzzusammenfassung
Disney+, ein Streaming-Dienst von Disney, der am 12. November 2019 gestartet hatte, wurde gehackt. Zugangsdaten wurden veröffentlicht und von einigen Benutzern, wurden die Zugangsdaten verändert, so dass die Besitzer nicht mehr auf ihr eigenes Konto zugreifen konnten.<br />
Bei anderen wurde versteckt einfach Serien mitgeschaut und es waren demnach fremde Inhalte im Verlauf.<br />
Im Darknet kann man die Daten zwischen 3 und 11 Dollar erwerben.<br />
Die Ursache ist noch nicht klar, wie es zu diesem Debakel gekommen ist. Mehr als 10 Millionen haben ein Konto und nur wenige Tausend Konten sind davon betroffen.<br />
Möglich wäre, dass durch die Sorglosigkeit des Benutzers, die Daten in Hände von Drittpersonen gelangt ist oder was plausibler wäre, Brute-Force-Attacken, Phishing-Seiten oder Trojaner.<br />

### Reflexion
Dieses Beispiel zeigt, wie schnell es gehen kann, bis die Daten weg sind. Es wiederspiegelt ebenfalls, wie viele Faktoren und Möglichkeiten es geben kann, wie zB. Sorglosigkeit, Brute-Force-Attacken, Phishing-Seiten oder Trojaner.<br />

### Was ist neu ? Was wusste ich schon ?
Mir ist neu, dass nicht klar ist, wie diese Daten in falsche Hände geraten sind und dass dies direkt beim Start des Dienstes passiert ist.<br />
Ich wusste schon, dass man heutzutage sehr vorsichtig sein muss und sichere Passwörter wählen sollte, damit solche Sachen nicht passieren.<br />

### Was hat sie erstaunt ?
Mich hat es erstaunt, dass vom Disney+ Support so wenig Rückmeldung gekommen ist, in so einer Situation sollte man alles daran setzen, dass die Kunden wieder zugriff auf ihr Konto bekommen und dass sie nicht über 3 Tage warten müssen und noch immer keine Antwort haben.<br />

### Wie schätze ich die Bedeutung dieser Information für mich persönlich, für meinen Beruf, für meinen Betrieb, für die Gesellschaft, ... ein?
Für mich persönlich zeigt es erneut, dass man sehr aufpassen muss, wem man seine Daten gibt und wie man sie sichert. <br /> 
Für meinen Beruf ist dies eine der grössten Herausforderungen, dass solche angriffe weniger werden und dass sie um einiges schwerer werden.<br />
Ich denke es ist für alle Betriebe ein Albtraum, wenn Firmen-Daten veröffentlicht oder gehackt werden, daher muss man sehr viele Sicherheitsmassnahmen ergreiffen.<br />

### Inwiefern werden Sie Ihr Verhalten/Handeln aufgrund dieser Information ändern bzw. anpassen?
Da mir dieses Thema nicht unbekannt ist und ich mich aufgrund immer wieder auftauchender Schlagzeilen damit auseinander gesetzt habe, muss ich nicht viel anpassen, da ich bereits gut aufpasse, wie ich mein Passwort usw sichere.<br />

# Spiegel

### Quelle
Die erste Quelle, die ich ausgewählt habe, ist von Spiegel.<br /> 
Die Quelle wirkt auf mich seriös, da es ein anerkenntes Magazin ist.<br />
Alle Informationen die für die Zusammenfassung weiter unten verwendet wurden, sind von dem folgenden Link: <br />
[https://www.spiegel.de/netzwelt/netzpolitik/universitaet-kiel-wird-von-cyber-attacke-gelaehmt-a-1296330.html](https://www.spiegel.de/netzwelt/netzpolitik/universitaet-kiel-wird-von-cyber-attacke-gelaehmt-a-1296330.html)

### Kurzzusammenfassung
Die Christian-Albrechts-Universität in Kiel wurde von einerer grösseren Cyberattacke zwei Wochen lang massiv gestört. Bei der Attacke handelt es sich um eine DDoS Attacke, bei der die Server überlastet werden.<br />
Der Onlinebetrieb der Uni war weitgehend lahmgelegt und es konnte weder auf E-Mails, Datenbanken, Studienunterlagen und teilweise sogar das Internet zugegriffen werden.<br />
Vertreter der Uni sagten, solche Angriffe wären nichts neues, aber das Ausmass(Heftigkeit und Länge) seien doch sehr ungewöhnlich gewesen.<br />
Der Onlinebetrieb lief in Wellenbewegungen, zehntausende Geräte waren davon betroffen aber es wurde versichert, dass der Angriff nicht mit dem Eindringen in das System verbunden ist.<br />
Zusätzlich wurde nun eine Anzeige erstattet, gegen den noch nicht bekannten Angreifer.<br />

### Reflexion
DDoS-Attacken können Systeme sehr schnell lahmlegen, wenn diese nicht gegen einen solchen Angriff geschützt sind, jedoch ist das Ausmass in diesem Fall erstaunlich hoch.<br />
Dass der Angriff über zwei Wochen dauerte, ist für eine solche Attacke ungewöhnlich.<br /> 

### Was ist neu ? Was wusste ich schon ?
Mir ist neu, dass dies solche Ausmasse annehmen kann und so lange andauern kann.<br />
Ich wusste jedoch, dass DDoS-Attacken ganze Konzerne lahmlegen kann.<br />

### Was hat sie erstaunt ?
Wie bereits weiter oben erwähnt hat mich sehr erstaunt, dass es mehr als zwei Wochen dauerte.<br />
Einerseits, dass es so lange dauerte bis die IT-Leute es in den Griff bekamen und andererseits, dass der Angriff nicht mit einem Datenklau verbunden worden ist.

### Wie schätze ich die Bedeutung dieser Information für mich persönlich, für meinen Beruf, für meinen Betrieb, für die Gesellschaft, ... ein?
DDoS-Attacken sind weniger geworden, da sich Unternehmen mehr dagegen schützen. Für meinen Beruf ist dies wichtig, da man sich immer auf dem neusten Stand halten sollte und daher auch sich gegen neue Methoden schützen sollte.<br />
Für meinen Betrieb ist es auch wichtig, dass man sich dagegen genügend gut schützt, da man sich ein solches Ausmass von einem Angriff nicht erlauben kann als Firma.<br />

### Inwiefern werden Sie Ihr Verhalten/Handeln aufgrund dieser Information ändern bzw. anpassen?
Mein Verhalten wird sich nicht verändern, ausser dass ich versuche Applikationen auch gegen dieses Ausmass an Angriffen zu schützen.
